package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	doAll()
}

func doAll() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter File Name: ")
	fileName, _ := reader.ReadString('\n')
	fmt.Print("Enter First Page Extent: ")
	firstExtent, _ := reader.ReadString('\n')
	fmt.Print("Enter Extent Gap: ")
	extentGap, _ := reader.ReadString('\n')
	fmt.Print("Enter Base Quantity: ")
	baseQty, _ := reader.ReadString('\n')
	fmt.Print("Enter Price: £")
	price, _ := reader.ReadString('\n')
	fmt.Print("Enter Runon Quantity: ")
	runonQty, _ := reader.ReadString('\n')
	fmt.Print("Enter Runon Price: £")
	runonPrice, _ := reader.ReadString('\n')

	fmt.Println(fileName)
	fmt.Println(firstExtent)
	fmt.Println(extentGap)
	fmt.Println(baseQty)
	fmt.Println(price)
	fmt.Println(runonQty)
	fmt.Println(runonPrice)

	for i := 0; i <= 56; i++ {

		intFirstExtent, _ := strconv.Atoi(strings.TrimSpace(firstExtent))
		intGap, _ := strconv.Atoi(strings.TrimSpace(extentGap))

		extentCalc := intFirstExtent + (i * intGap)

		fmt.Println(extentCalc)

	}

}
